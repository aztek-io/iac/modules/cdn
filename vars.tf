variable "sub_domain" {
  type    = string
  default = ""
}

variable "domain" {
  type = string
}

variable "tls" {
  type    = bool
  default = true
}

variable "index" {
  type    = string
  default = "index.html"
}

variable "error" {
  type    = string
  default = "error.html"
}
