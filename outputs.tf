output "domain_validation_options" {
  value = var.tls ? local.dvo : {}
}

output "s3_arn" {
  value = aws_s3_bucket.this.arn
}
