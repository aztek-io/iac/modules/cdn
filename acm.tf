resource "aws_acm_certificate" "this" {
  count    = var.tls ? 1 : 0
  provider = aws.dc

  domain_name       = local.domain
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "this_validation" {
  count   = var.tls ? 1 : 0
  name    = local.dvo["resource_record_name"]
  type    = local.dvo["resource_record_type"]
  zone_id = data.aws_route53_zone.this.id
  ttl     = 60
  records = [
    local.dvo["resource_record_value"]
  ]
}

resource "aws_acm_certificate_validation" "this" {
  count = var.tls ? 1 : 0

  provider        = aws.dc
  certificate_arn = aws_acm_certificate.this[count.index].arn

  validation_record_fqdns = [
    aws_route53_record.this_validation[count.index].fqdn
  ]
}
