resource "aws_s3_bucket" "this" {
  bucket        = local.domain
  acl           = "public-read"
  force_destroy = true

  website {
    index_document = var.index
    error_document = var.error
  }
}
