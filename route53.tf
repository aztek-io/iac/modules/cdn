resource "aws_route53_record" "s3_bucket" {
  count   = var.tls == false ? 1 : 0
  zone_id = data.aws_route53_zone.this.zone_id
  name    = local.domain
  type    = "A"

  alias {
    name                   = aws_s3_bucket.this.website_domain
    zone_id                = aws_s3_bucket.this.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "cloudfront" {
  count   = var.tls ? 1 : 0
  zone_id = data.aws_route53_zone.this.zone_id
  name    = local.domain
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.this[count.index].domain_name
    zone_id                = aws_cloudfront_distribution.this[count.index].hosted_zone_id
    evaluate_target_health = false
  }
}
