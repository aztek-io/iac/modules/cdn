locals {
  domain = length(var.sub_domain) > 0 ? "${var.sub_domain}.${var.domain}" : var.domain
  dvo    = tolist(aws_acm_certificate.this[0].domain_validation_options)[0]
}
