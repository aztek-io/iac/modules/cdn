# Module: cdn

Creates an s3 website with a cloudfront distribution in AWS.

## Example Usage

How to call this module:

```hcl
module "my_cdn" {
  source   = "git@gitlab.com:aztek-io/iac/modules/cdn.git?ref=v0.1.0"
  domain   = "example.com"
}
```

```hcl
module "my_other_cdn" {
  source     = "git@gitlab.com:aztek-io/iac/modules/cdn.git?ref=v0.1.0"
  sub_domain = "cdn"
  domain     = "example.com"
}
```

## Argument Reference

The following arguments are supported:

### Required Attributes

* `domain`     - (Optional|string) The name of domain to use.

### Optional Attributes

* `sub_domain` - (Optional|string) The name of sub domain to use. (Defaults to an empty string).
* `tls`        - (Optional|bool) Choose if tls for s3 website should be created. (Defaults to true).

## Attribute Reference

* `name` - The name of the cdn.
