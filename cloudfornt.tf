resource "aws_cloudfront_origin_access_identity" "this" {
  count = var.tls ? 1 : 0
}

resource "aws_cloudfront_distribution" "this" {
  count = var.tls ? 1 : 0
  origin {
    domain_name = aws_s3_bucket.this.bucket_regional_domain_name
    origin_id   = aws_cloudfront_origin_access_identity.this[count.index].id

    s3_origin_config {
      origin_access_identity = "origin-access-identity/cloudfront/${aws_cloudfront_origin_access_identity.this[count.index].id}"
    }
  }

  enabled             = true
  default_root_object = var.index

  custom_error_response {
    error_code         = "404"
    response_code      = "404"
    response_page_path = "/${var.error}"
  }

  aliases = [
    local.domain
  ]

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.this[count.index].arn
    minimum_protocol_version = "TLSv1.2_2018"
    ssl_support_method       = "sni-only"
  }

  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = aws_cloudfront_origin_access_identity.this[count.index].id
    viewer_protocol_policy = "redirect-to-https"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}
